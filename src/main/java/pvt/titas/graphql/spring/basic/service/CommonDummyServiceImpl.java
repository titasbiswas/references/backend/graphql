package pvt.titas.graphql.spring.basic.service;

import lombok.Data;
import pvt.titas.graphql.spring.basic.model.Address;
import pvt.titas.graphql.spring.basic.model.Department;
import pvt.titas.graphql.spring.basic.model.Employee;
import pvt.titas.graphql.spring.basic.model.Skill;

import java.util.*;
import java.util.stream.Collectors;


@Data
public class CommonDummyServiceImpl implements CommonDummyService{

    private Map<Long, Employee> empMap = new HashMap<>();
    private Map<Long, Department> depMap = new HashMap<>();
    private Map<Long, Skill> skillMap = new HashMap<>();
    private Map<Long, Address> addressMap = new HashMap<>();

    @Override
    public List<Skill> getSkillsByEmpId(Long empId) {
        return skillMap.values().stream().filter(f -> f.getEmpId()==empId).collect(Collectors.toList());
    }

    @Override
    public Optional<Skill> getSkillsById(Long skillId) {
        return Optional.ofNullable(skillMap.get(skillId));
    }

    @Override
    public List<Skill> getAllSkills() {
        return skillMap.values().stream().collect(Collectors.toList());
    }

    @Override
    public Optional<Department> getDepartmentsById(Long deptId) {
        return Optional.ofNullable(depMap.get(deptId));
    }

    @Override
    public Optional<Address> getAddressById(Long addressId) {
        return Optional.ofNullable(addressMap.get(addressId));
    }

    @Override
    public  Optional<Employee> getEmpById(Long empId) {
        return Optional.ofNullable(empMap.get(empId));
    }

    @Override
    public List<Employee> getAllEmployees() {
        return empMap.values().stream().collect(Collectors.toList());
    }

    @Override
    public Optional<Employee> saveEmployee(Long id, String firstName, String lastName, Long supervisorId, Long addressId, List<Long> departmentIds) {
        Employee newEmp = new Employee(id, firstName, lastName, supervisorId, addressId, departmentIds);
        if(empMap.containsKey(id)){
            empMap.put(id, newEmp);
        }
        else {
            newEmp.setId(empMap.size()+1L);
            empMap.put(empMap.size()+1L, newEmp);
        }
        return Optional.ofNullable(newEmp);
    }

}