package pvt.titas.graphql.spring.basic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pvt.titas.graphql.spring.basic.model.Skill;

import java.util.Arrays;
import java.util.List;


public interface SkillRepository
        //extends JpaRepository<Skill, Long>
{
    List<Skill> getSkillByEmpId(Long empId);
}
