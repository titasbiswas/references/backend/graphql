package pvt.titas.graphql.spring.basic.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Long id;
    private String firstName;
    private String lastName;
    private Long supervisorId;
    private Long addressId;
    private List<Long> departmentIds = new ArrayList<>();
}
