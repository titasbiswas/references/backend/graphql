package pvt.titas.graphql.spring.basic.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pvt.titas.graphql.spring.basic.model.Employee;
import pvt.titas.graphql.spring.basic.service.CommonDummyService;

import java.util.List;
import java.util.Optional;

@Component
public class EmployeeMutationResolver implements GraphQLMutationResolver {

    @Autowired
    private CommonDummyService commonDummyService;

    public Optional<Employee> createEmployee(String firstName, String lastName, Long supervisorId, Long addressId, List<Long> departmentIds){
        return commonDummyService.saveEmployee(null, firstName, lastName, supervisorId, addressId, departmentIds);
    }

    public Optional<Employee> updateEmployee(Long id, String firstName, String lastName, Long supervisorId, Long addressId, List<Long> departmentIds){
        return commonDummyService.saveEmployee(id, firstName, lastName, supervisorId, addressId, departmentIds);
    }
}
