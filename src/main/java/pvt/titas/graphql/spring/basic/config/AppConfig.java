package pvt.titas.graphql.spring.basic.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pvt.titas.graphql.spring.basic.model.Address;
import pvt.titas.graphql.spring.basic.model.Department;
import pvt.titas.graphql.spring.basic.model.Employee;
import pvt.titas.graphql.spring.basic.model.Skill;
import pvt.titas.graphql.spring.basic.service.CommonDummyService;
import pvt.titas.graphql.spring.basic.service.CommonDummyServiceImpl;

import java.util.Arrays;

@Configuration
public class AppConfig {

    @Bean
    public CommonDummyService commonDummyService(){
        CommonDummyServiceImpl commonDummyService = new CommonDummyServiceImpl();

        commonDummyService.getEmpMap().put(1L, new Employee(1L, "Harry", "Potter", 101L, 1L, Arrays.asList(1L, 2L)));
        commonDummyService.getEmpMap().put(101L, new Employee(101L, "Albus", "Dumbledore", 101L, 1L, Arrays.asList(1L)));

        commonDummyService.getAddressMap().put(1L, new Address("Hogwards", "Alnwick Castle", "560030", "Northumberland", "England"));

        commonDummyService.getSkillMap().put(1L, new Skill("Expecto patronum", 3.5, 101L));
        commonDummyService.getSkillMap().put(2L, new Skill("Obliviate", 3.5, 101L));
        commonDummyService.getSkillMap().put(3L, new Skill("Expelliarmus", 3.5, 1L));

        commonDummyService.getDepMap().put(1L, new Department(1L, "Gryffindor"));
        commonDummyService.getDepMap().put(2L,  new Department(2L, "Slytherin"));

        return commonDummyService;

    }
}
