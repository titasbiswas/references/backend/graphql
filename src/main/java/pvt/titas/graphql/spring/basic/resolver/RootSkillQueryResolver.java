package pvt.titas.graphql.spring.basic.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pvt.titas.graphql.spring.basic.model.Employee;
import pvt.titas.graphql.spring.basic.model.Skill;
import pvt.titas.graphql.spring.basic.service.CommonDummyServiceImpl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RootSkillQueryResolver implements GraphQLQueryResolver {
    @Autowired
    private CommonDummyServiceImpl commonDummyService;

    public Optional<Skill> getSkill(final Long skillId){
        return commonDummyService.getSkillsById(skillId);
    }

    public List<Skill> getSkills(){
        return commonDummyService.getAllSkills();
    }
}
