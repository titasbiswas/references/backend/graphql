package pvt.titas.graphql.spring.basic.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pvt.titas.graphql.spring.basic.model.Address;
import pvt.titas.graphql.spring.basic.model.Department;
import pvt.titas.graphql.spring.basic.model.Employee;
import pvt.titas.graphql.spring.basic.model.Skill;
import pvt.titas.graphql.spring.basic.service.CommonDummyServiceImpl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class EmployeeResolver implements GraphQLResolver<Employee> {

    @Autowired
    private CommonDummyServiceImpl service;

    public List<Department> getDepartments(Employee employee) {
        return employee.getDepartmentIds().stream()
                .map(d -> service.getDepartmentsById(d))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    public List<Skill> getSkills(Employee employee) {

        return service.getSkillsByEmpId(employee.getId());
    }

    public Optional<Address> getAddress(Employee employee) {

        return service.getAddressById(employee.getAddressId());
    }

}
