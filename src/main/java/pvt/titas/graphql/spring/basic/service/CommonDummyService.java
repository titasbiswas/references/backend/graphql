package pvt.titas.graphql.spring.basic.service;

import pvt.titas.graphql.spring.basic.model.Address;
import pvt.titas.graphql.spring.basic.model.Department;
import pvt.titas.graphql.spring.basic.model.Employee;
import pvt.titas.graphql.spring.basic.model.Skill;

import java.util.List;
import java.util.Optional;

public interface CommonDummyService {


    List<Skill> getSkillsByEmpId(Long empId);

    Optional<Skill> getSkillsById(Long skillId);

    List<Skill> getAllSkills();

    Optional<Department> getDepartmentsById(Long deptId);

    Optional<Address> getAddressById(Long addressId);
    
    Optional<Employee> getEmpById(Long empId);

    List<Employee> getAllEmployees();

    Optional<Employee> saveEmployee(Long id, String firstName, String lastName, Long supervisorId, Long addressId, List<Long> departmentIds);
}
