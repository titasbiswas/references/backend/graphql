package pvt.titas.graphql.spring.basic.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pvt.titas.graphql.spring.basic.model.Employee;
import pvt.titas.graphql.spring.basic.service.CommonDummyServiceImpl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RootEmployeeQueryResolver implements GraphQLQueryResolver {

    @Autowired
    private CommonDummyServiceImpl commonDummyService;

    public Optional<Employee> getEmployee(final Long empId){
        return commonDummyService.getEmpById(empId);
    }

    public List<Employee> getEmployees(final Long limit){
        return commonDummyService.getAllEmployees().stream().limit(limit).collect(Collectors.toList());
    }
}
